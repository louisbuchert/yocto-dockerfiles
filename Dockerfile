FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_US.UTF-8

ARG USERNAME=dev
ARG UID=1000
ARG GID=1000

RUN apt-get update && apt-get install -y \
    locales vim

# Yocto specific
RUN apt-get update && apt-get install -y \
    gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python3 python3-pip \
    python3-pexpect xz-utils debianutils iputils-ping python3-git \
    python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm \
    python3-subunit mesa-common-dev

RUN apt-get install -y \
    locales vim gcc-8 g++-8

RUN groupadd -g ${GID} ${USERNAME} \
    && useradd -u ${UID} -N -g ${GID} -m ${USERNAME} \
    && echo "root:root" | chpasswd \
    && locale-gen en_US.UTF-8

USER ${USERNAME}:${USERNAME}

