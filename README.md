Ubuntu based Yocto Dockerfiles.

Switch to your chosen branch.

Build and run commands:
```
$ make build
$ make run
```
Which is the same as:
```
$ make
```
The directory a level up from this one will be bound.

